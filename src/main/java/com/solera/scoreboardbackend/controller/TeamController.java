package com.solera.scoreboardbackend.controller;

import com.solera.scoreboardbackend.domain.model.Team;
import com.solera.scoreboardbackend.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TeamController {

    @Autowired
    private TeamService teamService;

    @GetMapping(path = "/teams", produces = "application/json")
    public List<Team> getTeams() {
        return teamService.findAll();
    }


}
