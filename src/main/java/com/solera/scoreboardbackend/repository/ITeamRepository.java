package com.solera.scoreboardbackend.repository;

import com.solera.scoreboardbackend.domain.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITeamRepository extends JpaRepository<Team, Long> {
}
