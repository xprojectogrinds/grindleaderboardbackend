package com.solera.scoreboardbackend.repository;

import com.solera.scoreboardbackend.domain.model.ActivitiesTeams;
import com.solera.scoreboardbackend.domain.model.Activity;
import com.solera.scoreboardbackend.domain.model.Team;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface IActivitiesTeamsRepository extends JpaRepository<ActivitiesTeams, Long> {
}
