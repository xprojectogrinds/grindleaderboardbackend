package com.solera.scoreboardbackend.repository;

import com.solera.scoreboardbackend.domain.model.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IActivityRepository extends JpaRepository <Activity, Long>{
}
