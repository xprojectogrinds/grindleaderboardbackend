package com.solera.scoreboardbackend.domain.model;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;

@Data
@Entity
@Table (name = "activity_team")
@ResponseBody
@IdClass(ActivitiesTeamsId.class)
public class ActivitiesTeams implements Serializable{
    @Id
    @ManyToOne
    private Team team;
    @Id
    @ManyToOne
    private Activity activity;
    @Column(nullable = false)
    private int score;
}
