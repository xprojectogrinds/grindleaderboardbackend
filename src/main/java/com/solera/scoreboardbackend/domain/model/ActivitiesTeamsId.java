package com.solera.scoreboardbackend.domain.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ActivitiesTeamsId implements Serializable {
    private Long activity;
    private Long team;
}
