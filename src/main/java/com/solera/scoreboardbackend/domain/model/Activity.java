package com.solera.scoreboardbackend.domain.model;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;

@Entity
@Table(name = "activities")
@Data
@ResponseBody
public class Activity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

}
