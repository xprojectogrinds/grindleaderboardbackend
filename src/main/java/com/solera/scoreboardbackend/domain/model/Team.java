package com.solera.scoreboardbackend.domain.model;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Set;

@Table (name = "teams")
@Entity
@Data
@ResponseBody
public class Team implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private int total_points;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "team")
    private Set<ActivitiesTeams> activities;

}
