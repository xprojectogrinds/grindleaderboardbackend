package com.solera.scoreboardbackend.service;

import com.solera.scoreboardbackend.domain.model.ActivitiesTeams;
import com.solera.scoreboardbackend.repository.IActivitiesTeamsRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class TeamsActivitiesService extends CommonService<ActivitiesTeams, IActivitiesTeamsRepository> {
}
