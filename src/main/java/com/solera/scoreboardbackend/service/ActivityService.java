package com.solera.scoreboardbackend.service;

import com.solera.scoreboardbackend.domain.model.Activity;
import com.solera.scoreboardbackend.domain.model.Team;
import com.solera.scoreboardbackend.repository.IActivityRepository;
import com.solera.scoreboardbackend.repository.ITeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class ActivityService extends CommonService<Activity, IActivityRepository>{
}
