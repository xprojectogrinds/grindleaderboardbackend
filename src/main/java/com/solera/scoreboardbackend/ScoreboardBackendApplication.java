package com.solera.scoreboardbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.*;

@SpringBootApplication
public class ScoreboardBackendApplication {

	public static void main(String[] args) throws SQLException {
		SpringApplication.run(ScoreboardBackendApplication.class, args);
	}

}
